#!/bin/bash

echo Installed software
echo
echo Rust
echo ===========
rustc --version
echo
echo Cargo
echo ===========
cargo --version
echo
